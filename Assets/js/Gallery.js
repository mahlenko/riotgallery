// GalleryShow Script
_GalleryShowAlbum = []; // List Albums
var GalleryShow = {

    init: function (obj) {
        var $_object = $(obj);

        // Create box
        GalleryShow.CreateBox();

        $.each($_object.find('[data-src]'), function(){
            // Append photo to albums
            GalleryShow.AddPhotoForAlbums(this);

            // Action click photos
            GalleryShow.AddAction(this);
        });

        $('.RiotGalleryControlPrev').click(function(){
            return GalleryShow.Prev();
        });

        $('.RiotGalleryControlNext').click(function() {
            return GalleryShow.Next();
        });

        $('.RiotGalleryControlClose').click(function(){
            return GalleryShow.Close();
        });

        $(window).resize(function(){
            GalleryShow.VerticalPosition();
        });
    },


    // Append photos to album
    AddPhotoForAlbums : function(image) {
        var PhotoSrc = $(image).data('src');
        _GalleryShowAlbum.push(PhotoSrc);
    },


    // Add action click
    AddAction: function (image) {
        $(image).css({'cursor' : 'pointer'})
            .click(function(){
                GalleryShow.Show(this);
            });
    },


    // Create Box
    CreateBox : function() {
        $background = $('<div class="RiotGalleryBackground"></div>');
        $imagebox   = $('<div class="RiotGalleryImageBox"><img src="" alt=""></div>');
        $control    = $('<span class="RiotGalleryControlPrev" data-control="prev"></span><span class="RiotGalleryControlNext" data-control="next"></span><span class="RiotGalleryControlClose" data-control="close"></span>');

        $background.click(function(e){
            if ( $(e.target).is('div.RiotGalleryImageBox') ) {
                GalleryShow.Close();
            }
        });

        $imagebox.appendTo($background);
        $control.appendTo($background);
        $background.appendTo('body').hide();
    },


    Show: function (image) {
        
        if ( typeof image == 'object' ) {
            var ImageSrc = $(image).data('src');
        } else {
            var ImageSrc = image;
        }
        
        $imagebox.find('img').prop('src', ImageSrc).hide();
        $imagebox.find('img').load(function(){
            // Get size
            GalleryShow.VerticalPosition();                        
            $(this).fadeIn(150);
        });

        $background.show();
        $('body').css('overflow', 'hidden');

        $(document).keyup(function(e){
            GalleryShow.KeyAction(e);
        });

    },

    // Показать фотографию по ее номеру
    Number : function(number) {
        $imagebox.find('img').fadeOut(100, function(){
            $imagebox.find('img').attr('src', _GalleryShowAlbum[number]).load(function(){
                $(this).fadeIn();
            });
        });
    },


    Prev : function(){
        var ImageSrc = $imagebox.find('img').attr('src');
        var KeyImage = _GalleryShowAlbum.indexOf(ImageSrc);

        var IndexShow = KeyImage - 1;
        if (IndexShow < 0) return false;

        GalleryShow.Number(IndexShow);  
    },


    Next : function(){
        var ImageSrc = $imagebox.find('img').attr('src');
        var KeyImage = _GalleryShowAlbum.indexOf(ImageSrc);

        var IndexShow = KeyImage + 1;
        if (IndexShow > _GalleryShowAlbum.length-1) return false;

        console.log(IndexShow);
        GalleryShow.Number(IndexShow); 
    },


    Close: function(){
        $imagebox.find('img').fadeOut(250, function() {
            $(this).attr('src', null);
        });
        $background.fadeOut(50);                        

        $('body').css('overflow', 'auto');
        $(document).unbind('keyup');
    },


    VerticalPosition : function()
    {
        var MarginTop = (window.innerHeight - $imagebox.find('img').height()) / 2;
        $imagebox.find('img').css('margin-top', MarginTop);
    },


    KeyAction: function(e) {
        switch(e.keyCode)
        {
            case 27: // Esc
                GalleryShow.Close();
                break;

            case 37: // arrow left
                GalleryShow.Prev();
                break;

            case 39: // arrow left
                GalleryShow.Next();
                break;
        }
    }

}